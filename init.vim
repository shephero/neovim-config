""""""""""""""""""""""""""""""
"                            " 
"                            " 
"    Owen's Neovim Config    " 
"                            " 
"                            " 
""""""""""""""""""""""""""""""

" Languages given attention:
" * Haskell
" * Rust
" * C
"
" Prequisites:
" * Neovim
" * Dein
" * Python3

"""""""""""""""""""""""
"                     "
"  Plugin Management  "
"                     "
"""""""""""""""""""""""

set runtimepath^=/home/owen/.config/nvim/repos/github.com/Shougo/dein.vim
call dein#begin(expand('/home/owen/.config/nvim'))
call dein#add('Shougo/dein.vim')
call dein#add('Shougo/neosnippet.vim')
call dein#add('Shougo/neosnippet-snippets')
call dein#add('Shougo/deoplete.nvim')
call dein#add('eagletmt/neco-ghc')
call dein#add('mattn/emmet-vim')
call dein#end()
if dein#check_install()
  call dein#install()
endif

""""""""""""""""
"              "
"  Completion  "
"              "
""""""""""""""""

let g:deoplete#enable_at_startup = 1

""""""""""""
"          "
"  Basics  "
"          "
""""""""""""

filetype plugin indent on
set number
syntax on
colorscheme desert
set backspace=indent,eol,start

" Auto reload .vimrc on save
if has("autocmd")
  autocmd bufwritepost $MYVIMRC source $MYVIMRC
endif

""""""""""""""""
"              "
"  Formatting  "
"              "
""""""""""""""""

set columns=80
set textwidth=80
set expandtab
set tabstop=4
set shiftwidth=4

"""""""""""""""""
"               "
"  Keybindings  "
"               "
"""""""""""""""""

let mapleader = "\\"
" Edit this config
nmap <leader>v :edit $MYVIMRC<CR>
" Insert Date
imap <leader>d <C-R>=strftime('%a %d %b %Y')<CR>
" Insert Time
imap <leader>t <C-R>=strftime('%T')<CR>


